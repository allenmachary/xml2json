xml2json
-------
Convert XML to JSON

License: [http://creativecommons.org/licenses/LGPL/2.1/](http://creativecommons.org/licenses/LGPL/2.1/)

Version: 0.9

Author:  Stefan Goessner/2006

Web:     [http://goessner.net/ ](http://goessner.net/ )

Link: [http://goessner.net/download/prj/jsonxml/xml2json.js](http://goessner.net/download/prj/jsonxml/xml2json.js)